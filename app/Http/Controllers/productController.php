<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class productController extends Controller
{
    public function show($id)
    {
        $post = DB::table('detail_product')->where('id', $id)->first();
        return view('product.productview', compact('post'));
    }

}
